import React, { Component } from 'react';
import {Route, Switch} from "react-router-dom";

import Layout from "./containers/Layout/Layout";
import Artists from "./containers/Artists/Artists";
import NewArtist from "./containers/Artists/NewArtist";
import Albums from "./containers/Albums/Albums";
import NewAlbum from "./containers/Albums/NewAlbum";
import Tracks from "./containers/Tracks/Tracks";
import NewTrack from "./containers/Tracks/NewTrack";
import History from "./containers/History/History";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={Artists} />
          <Route path="/artists/new" exact component={NewArtist} />
          <Route path="/albums/new" exact component={NewAlbum} />
          <Route path="/tracks/new" exact component={NewTrack} />
          <Route path="/albums/:id" component={Albums} />
          <Route path="/tracks/:id" component={Tracks} />
          <Route path="/history" component={History} />
          <Route path="/register" exact component={Register} />
          <Route path="/login" exact component={Login} />
        </Switch>
      </Layout>
    );
  }
}

export default App;