import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import NewArtistForm from "../../components/Forms/NewArtistForm";
import {createArtist} from "../../store/actions/artists";

class NewArtist extends Component {
  createArtist = artistData => {
    this.props.onArtistCreated(artistData).then(() => {
      this.props.history.push('/');
    });
  };

  render() {
    return (
      <Fragment>
        <PageHeader>New artist</PageHeader>
        <NewArtistForm onSubmit={this.createArtist} />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onArtistCreated: artistData => dispatch(createArtist(artistData))
});

export default connect(null, mapDispatchToProps)(NewArtist);