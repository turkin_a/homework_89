import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";
import {Link} from "react-router-dom";

import ArtistsListItem from "../../components/ArtistsListItem/ArtistsListItem";
import {fetchArtists} from "../../store/actions/artists";

class Artists extends Component {
  componentDidMount() {
    this.props.onFetchArtists();
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          Artists
          {this.props.user ?
            <Link to="/artists/new">
              <Button bsStyle="primary" className="pull-right">
                Add new artist
              </Button>
            </Link>
          : null}
        </PageHeader>
        {this.props.artists.map(artist => (
          <ArtistsListItem
            key={artist._id}
            id={artist._id}
            name={artist.name}
            photo={artist.photo}
            published={artist.published}
            user={this.props.user}
          />
        ))}
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user,
    artists: state.artists.artists
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchArtists: () => dispatch(fetchArtists())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Artists);