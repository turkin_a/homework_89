import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import NewAlbumForm from "../../components/Forms/NewAlbumForm";
import {createAlbum} from "../../store/actions/albums";

class NewAlbum extends Component {
  createAlbum = (albumData, artistId) => {
    this.props.onAlbumCreated(albumData).then(() => {
      this.props.history.push(`/albums/${artistId}`);
    });
  };

  render() {
    return (
      <Fragment>
        <PageHeader>New album</PageHeader>
        <NewAlbumForm onSubmit={this.createAlbum} />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onAlbumCreated: albumData => dispatch(createAlbum(albumData))
});

export default connect(null, mapDispatchToProps)(NewAlbum);