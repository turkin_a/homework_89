import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";
import {Link} from "react-router-dom";

import {fetchAlbums, publishAlbum, removeAlbum} from "../../store/actions/albums";
import AlbumsListItem from "../../components/AlbumsListItem/AlbumsListItem";

class Albums extends Component {
  componentDidMount() {
    this.props.onFetchAlbums(this.props.match.params.id);
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          Albums
          {this.props.user ?
            <Link to="/albums/new">
              <Button bsStyle="primary" className="pull-right">
                Add new album
              </Button>
            </Link>
          : null}
        </PageHeader>
        {this.props.albums.map(album => (
          <AlbumsListItem
            key={album._id}
            id={album._id}
            title={album.title}
            artist={album.artist}
            year={album.year}
            poster={album.poster}
            published={album.published}
            user={this.props.user}
            publish={this.props.onPublishAlbum}
            remove={this.props.onRemoveAlbum}
          />
        ))}
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user,
    albums: state.albums.albums
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchAlbums: (id) => dispatch(fetchAlbums(id)),
    onPublishAlbum: (id) => dispatch(publishAlbum(id)),
    onRemoveAlbum: (id) => dispatch(removeAlbum(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Albums);