import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import {fetchHistory} from "../../store/actions/history";
import HistoryListItem from "../../components/HistoryListItem/HistoryListItem";

class History extends Component {
  componentDidMount() {
    this.props.onFetchHistory();
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          Tracks history
        </PageHeader>
        {this.props.tracks.map(track => (
          <HistoryListItem
            key={track._id}
            id={track._id}
            name={track.name}
            artist={track.artist}
            datetime={track.datetime}
          />
        ))}
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    tracks: state.history.tracks
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchHistory: () => dispatch(fetchHistory())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(History);