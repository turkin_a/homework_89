import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";
import {Link} from "react-router-dom";

import {fetchTracks} from "../../store/actions/tracks";
import TracksListItem from "../../components/TracksListItem/TracksListItem";
import {addToHistory} from "../../store/actions/history";

class Tracks extends Component {
  componentDidMount() {
    this.props.onFetchTracks(this.props.match.params.id);
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          Tracks
          {this.props.user ?
            <Link to="/tracks/new">
              <Button bsStyle="primary" className="pull-right">
                Add new track
              </Button>
            </Link>
          : null}
        </PageHeader>
        {this.props.tracks.map(track => (
          <TracksListItem
            key={track._id}
            id={track._id}
            name={track.name}
            album={track.album}
            duration={track.duration}
            number={track.number}
            published={track.published}
            user={this.props.user}
            clicked={() => this.props.addToHistory(track._id)}
          />
        ))}
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user,
    tracks: state.tracks.tracks
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchTracks: (id) => dispatch(fetchTracks(id)),
    addToHistory: (id) => dispatch(addToHistory(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);