import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import NewTrackForm from "../../components/Forms/NewTrackForm";
import {createTrack} from "../../store/actions/tracks";

class NewTrack extends Component {
  createTrack = (trackData, albumId) => {
    console.log(albumId);
    this.props.onTrackCreated(trackData).then(() => {
      this.props.history.push(`/tracks/${albumId}`);
    });
  };

  render() {
    return (
      <Fragment>
        <PageHeader>New track</PageHeader>
        <NewTrackForm onSubmit={this.createTrack} />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onTrackCreated: trackData => dispatch(createTrack(trackData))
});

export default connect(null, mapDispatchToProps)(NewTrack);