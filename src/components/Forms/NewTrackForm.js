import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Col, Form, FormGroup} from "react-bootstrap";

import FormElement from "../UI/Form/FormElement";
import {fetchArtists} from "../../store/actions/artists";
import {fetchAlbums} from "../../store/actions/albums";

class NewTracksForm extends Component {
  state = {
    name: '',
    duration: '',
    number: '',
    artist: '',
    album: ''
  };

  componentDidMount() {
    this.props.onFetchArtists();
  }

  submitFormHandler = event => {
    event.preventDefault();

    let formData = {...this.state};
    delete formData['artist'];

    this.props.onSubmit(formData, this.state.album);
  };

  inputChangeHandler = event => {
    if (event.target.name === 'artist' && event.target.value) {
      this.props.onFetchAlbums(event.target.value)
    }

    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
    const currentAlbums = this.state.artist ? this.props.albums : [];

    return (
      <Form horizontal onSubmit={this.submitFormHandler}>

        <FormElement
          propertyName="name"
          title="Track title"
          type="text"
          value={this.state.name}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="duration"
          title="Duration (sec)"
          type="text"
          value={this.state.duration}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="number"
          title="Track number"
          type="text"
          value={this.state.number}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="artist"
          title="Select Artist"
          type="select"
          options={this.props.artists}
          value={this.state.artist}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="album"
          title="Select Album"
          type="select"
          options={currentAlbums}
          value={this.state.album}
          changeHandler={this.inputChangeHandler}
          required
          disabled={!this.state.artist}
        />

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    artists: state.artists.artists,
    albums: state.albums.albums
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchArtists: () => dispatch(fetchArtists()),
    onFetchAlbums: (id) => dispatch(fetchAlbums(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewTracksForm);