import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import FormElement from "../UI/Form/FormElement";

class NewArtistForm extends Component {
  state = {
    name: '',
    info: '',
    photo: ''
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>

        <FormElement
          propertyName="name"
          title="Artist name"
          type="text"
          value={this.state.name}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="info"
          title="Artist description"
          type="text"
          value={this.state.info}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="photo"
          title="Artist photo"
          type="file"
          changeHandler={this.fileChangeHandler}
          required
        />

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default NewArtistForm;