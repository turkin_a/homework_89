import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Col, Form, FormGroup} from "react-bootstrap";

import FormElement from "../UI/Form/FormElement";
import {fetchArtists} from "../../store/actions/artists";

class NewAlbumForm extends Component {
  state = {
    title: '',
    year: '',
    artist: '',
    poster: ''
  };

  componentDidMount() {
    this.props.onFetchArtists();
  }

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData, this.state.artist);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>

        <FormElement
          propertyName="title"
          title="Album title"
          type="text"
          value={this.state.title}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="year"
          title="Year"
          type="text"
          value={this.state.year}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="artist"
          title="Select Artist"
          type="select"
          options={this.props.artists}
          value={this.state.artist}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="poster"
          title="Album poster"
          type="file"
          changeHandler={this.fileChangeHandler}
          required
        />

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    artists: state.artists.artists
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchArtists: () => dispatch(fetchArtists())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewAlbumForm);