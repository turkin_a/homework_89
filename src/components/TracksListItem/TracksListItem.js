import React from 'react';
import {Panel} from "react-bootstrap";
import PropTypes from 'prop-types';

const TracksListItem = props => {
  let control = (
    <p>
      {props.published ? null :
        <strong
          style={{color: 'red', cursor: 'pointer'}}
          onClick={() => {}}
        >Not published</strong>}
    </p>
  );

  if (props.user) {
    if (props.user.role === 'admin') {
      control = (
        <p>
          {props.published ? null :
            <strong
              style={{color: 'blue', cursor: 'pointer', marginRight: '15px'}}
              onClick={() => {}}
            >Publish</strong>}
          <strong
            style={{color: 'red', cursor: 'pointer'}}
            onClick={() => {}}
          >Remove</strong>
        </p>
      );
    }
  }

  return (
    <Panel>
      <Panel.Body>
        <p>Track #{props.number}</p>
        <p>
          <strong style={{marginRight: '15px', cursor: 'pointer'}} onClick={props.clicked}>{props.name}</strong>
          {`Duration: ${Math.floor(props.duration / 60)}:${props.duration % 60}`}
        </p>
        {control}
      </Panel.Body>
    </Panel>
  );
};

TracksListItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  album: PropTypes.string.isRequired,
  duration: PropTypes.string.isRequired,
  number: PropTypes.number.isRequired,
  published: PropTypes.bool
};

export default TracksListItem;