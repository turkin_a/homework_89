import React from 'react';
import {Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import notFound from '../../assets/images/not_found.png';
import config from "../../config";

const ArtistsListItem = props => {
  let photo = notFound;

  if (props.photo) {
    photo = config.apiUrl + '/uploads/' + props.photo;
  }

  let control = (
    <p>
      {props.published ? null :
        <strong
          style={{color: 'red', cursor: 'pointer'}}
          onClick={() => {}}
        >Not published</strong>}
    </p>
  );

  if (props.user) {
    if (props.user.role === 'admin') {
      control = (
        <p>
          {props.published ? null :
            <strong
              style={{color: 'blue', cursor: 'pointer', marginRight: '15px'}}
              onClick={() => {}}
            >Publish</strong>}
          <strong
            style={{color: 'red', cursor: 'pointer'}}
            onClick={() => {}}
          >Remove</strong>
        </p>
      );
    }
  }

  return (
    <Panel>
      <Panel.Body>
        <Image
          style={{width: '100px', marginRight: '10px', float: 'left'}}
          src={photo}
          thumbnail
        />
        <Link to={'/albums/' + props.id}>
          <strong>{props.name}</strong>
        </Link>
        {control}
      </Panel.Body>
    </Panel>
  );
};

ArtistsListItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  photo: PropTypes.string.isRequired,
  published: PropTypes.bool
};

export default ArtistsListItem;