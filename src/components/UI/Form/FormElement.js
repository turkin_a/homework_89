import React from 'react';
import PropTypes from 'prop-types';
import {Col, ControlLabel, FormControl, FormGroup, HelpBlock} from "react-bootstrap";

const FormElement = props => {
  let componentClass, formControlChildren;

  if (props.type === 'select') {
    componentClass = 'select';

    try {
      if (props.options[0]._id !== '') {
        props.options.unshift({_id: '', title: '--Please ' + props.title + '--'});
      }
    } catch (e) {}

    formControlChildren = props.options.map(element => (
      <option key={element._id} value={element._id}>{element.title || element.name}</option>
    ));
  }

  return (
    <FormGroup
      controlId={props.propertyName}
      validationState={props.error && 'error'}
    >
      <Col componentClass={ControlLabel} sm={2}>
        {props.title}
      </Col>
      <Col sm={10}>
        <FormControl
          type={props.type}
          componentClass={componentClass}
          placeholder={props.placeholder}
          name={props.propertyName}
          value={props.value}
          onChange={props.changeHandler}
          required={props.required}
          autoComplete={props.autoComplete}
          disabled={props.disabled}
        >
          {formControlChildren}
        </FormControl>
        {props.error &&
          <HelpBlock>{props.error}</HelpBlock>
        }
      </Col>
    </FormGroup>
  )
};

FormElement.PropTypes = {
  propertyName: PropTypes.string.isRequired,
  error: PropTypes.string,
  title: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  changeHandler: PropTypes.func.isRequired,
  required: PropTypes.bool,
  autoComplete: PropTypes.string,
  disabled: PropTypes.bool
};

export default FormElement;