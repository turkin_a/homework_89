import React, {Fragment} from 'react';
import {MenuItem, Nav, Navbar, NavDropdown, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {connect} from "react-redux";

import {logoutUser} from "../../../store/actions/users";

const Toolbar = props => (
  <Navbar>
    <Navbar.Header>
      <Navbar.Brand>
        <LinkContainer to="/" exact><a>Music</a></LinkContainer>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav pullRight>
        {props.user ?
          <Fragment>
            <NavItem disabled>Hello, {props.user.username}!</NavItem>
            <NavDropdown title="Tools" id="user-menu">
              <LinkContainer to="/history">
                <NavItem>See your history</NavItem>
              </LinkContainer>
              <MenuItem divider />
              <LinkContainer to="/artists/new">
                <NavItem>Add new artist</NavItem>
              </LinkContainer>
              <LinkContainer to="/albums/new">
                <NavItem>Add new album</NavItem>
              </LinkContainer>
              <LinkContainer to="/tracks/new">
                <NavItem>Add new track</NavItem>
              </LinkContainer>
              <MenuItem divider />
              <MenuItem onClick={props.logoutUser}>Logout</MenuItem>
            </NavDropdown>
          </Fragment>
          :
          <Fragment>
            <LinkContainer to="/register" exact>
              <NavItem>Sign up</NavItem>
            </LinkContainer>
            <LinkContainer to="/login" exact>
              <NavItem>Login</NavItem>
            </LinkContainer>
          </Fragment>
        }
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser())
});

export default connect(null, mapDispatchToProps)(Toolbar);