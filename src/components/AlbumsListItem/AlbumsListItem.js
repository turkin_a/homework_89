import React from 'react';
import {Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import notFound from '../../assets/images/not_found.png';
import config from "../../config";

const AlbumsListItem = props => {
  let poster = notFound;

  if (props.poster) {
    poster = config.apiUrl + '/uploads/' + props.poster;
  }

  let control = (
    <p>
      {props.published ? null :
        <strong
          style={{color: 'red'}}
        >Not published</strong>}
    </p>
  );

  if (props.user) {
    if (props.user.role === 'admin') {
      control = (
        <p>
          {props.published ? null :
            <strong
              style={{color: 'blue', cursor: 'pointer', marginRight: '15px'}}
              onClick={() => props.publish(props.id)}
            >Publish</strong>}
          <strong
            style={{color: 'red', cursor: 'pointer'}}
            onClick={() => props.remove(props.id)}
          >Remove</strong>
        </p>
      );
    }
  }

  return (
    <Panel>
      <Panel.Body>
        <Link to={'/tracks/' + props.id}>
          <Image
            style={{width: '120px', marginRight: '10px', float: 'left'}}
            src={poster}
            thumbnail
          />
        </Link>
        <Link to={'/tracks/' + props.id}>
          <strong>{props.title}</strong>
        </Link>
        <p>Year: {props.year}</p>
        {control}
      </Panel.Body>
    </Panel>
  );
};

AlbumsListItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  artist: PropTypes.string.isRequired,
  year: PropTypes.string.isRequired,
  poster: PropTypes.string.isRequired,
  published: PropTypes.bool
};

export default AlbumsListItem;