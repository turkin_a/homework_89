import axios from '../../axios-api';
import {CREATE_TRACK_SUCCESS, FETCH_TRACKS_SUCCESS} from "./actionTypes";

const fetchTracksSuccess = tracks => {
  return {type: FETCH_TRACKS_SUCCESS, tracks};
};

export const fetchTracks = id => {
  return (dispatch, getState) => {
    let token = '';

    if (getState().users.user) {
      token = getState().users.user.token;
    }

    const headers = {
      'Token': token
    };

    return axios.get('/tracks/' + id, {headers}).then(
      response => dispatch(fetchTracksSuccess(response.data))
    )
  };
};

export const createTrackSuccess = () => {
  return {type: CREATE_TRACK_SUCCESS};
};

export const createTrack = trackData => {
  return (dispatch, getState) => {
    const headers = {
      'Token': getState().users.user.token
    };

    return axios.post('/tracks', trackData, {headers}).then(
      response => dispatch(createTrackSuccess())
    );
  };
};