import axios from '../../axios-api';
import {CREATE_ARTIST_SUCCESS, FETCH_ARTISTS_SUCCESS} from "./actionTypes";

const fetchArtistsSuccess = artists => {
  return {type: FETCH_ARTISTS_SUCCESS, artists};
};

export const fetchArtists = () => {
  return (dispatch, getState) => {
    let token = '';

    if (getState().users.user) {
      token = getState().users.user.token;
    }

    const headers = {
      'Token': token
    };

    return axios.get('/artists', {headers}).then(
      response => {
        dispatch(fetchArtistsSuccess(response.data));
      }
    )
  };
};

export const createArtistSuccess = () => {
  return {type: CREATE_ARTIST_SUCCESS};
};

export const createArtist = artistData => {
  return (dispatch, getState) => {
    const headers = {
      'Token': getState().users.user.token
    };

    return axios.post('/artists', artistData, {headers}).then(
      response => dispatch(createArtistSuccess())
    );
  };
};