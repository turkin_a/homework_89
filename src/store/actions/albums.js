import axios from '../../axios-api';
import {CREATE_ALBUM_SUCCESS, FETCH_ALBUMS_SUCCESS} from "./actionTypes";

const fetchAlbumsSuccess = albums => {
  return {type: FETCH_ALBUMS_SUCCESS, albums};
};

export const fetchAlbums = id => {
  return (dispatch, getState) => {
    let token = '';

    if (getState().users.user) {
      token = getState().users.user.token;
    }

    const headers = {
      'Token': token
    };

    return axios.get('/albums/' + id, {headers}).then(
      response => {
        dispatch(fetchAlbumsSuccess(response.data))
      }
    )
  };
};

const createAlbumSuccess = () => {
  return {type: CREATE_ALBUM_SUCCESS};
};

export const createAlbum = albumData => {
  return (dispatch, getState) => {
    const headers = {
      'Token': getState().users.user.token
    };

    return axios.post('/albums', albumData, {headers}).then(
      response => dispatch(createAlbumSuccess())
    );
  };
};

export const publishAlbum = id => {
  return (dispatch, getState) => {
    const headers = {
      'Token': getState().users.user.token
    };

    return axios.post(`/albums/${id}/publish`, {headers}).then(
      response => dispatch(fetchAlbums(response.data.artist))
    );
  };
};

export const removeAlbum = id => {
  return (dispatch, getState) => {
    const headers = {
      'Token': getState().users.user.token
    };

    return axios.delete(`/albums/${id}`, {headers}).then(
      response => dispatch(fetchAlbums(response.data))
    );
  };
};